<!DOCTYPE html>
<h1 align="center">
  <br>
  <img src="https://www.filecluster.com/howto/wp-content/uploads/2016/02/CMD-Windows-10.png" alt="Random Batch Codes">
  <br>
  Batch Scripts by the best in the world
  <br>
</h1>
<h1 align="center" style="background-color:#bdbdbd">
  <br>
  Thank You for coming!
</h1>
  
This repository is for my random scripts (Primarily CMD/Batch scripts) that I create while I'm working on coding in general.
I plan to get a good grip on batch file making and understanding the windows command prompt.
Take a look at some of the codes yourself if you'd like! **None are harmful to any part of Windows or any files!**
You can download the scripts from the repository here on GitLab! (All browsers will say it's harmful but I promise they're not. It says that since they're batch scripts and windows can't check to see what the code does by itself so it deems it unsafe. If there's otherwise then please contact me!)
Anyway thanks for coming to this page in the 
first place, I appreciate it!

<h2 align="center">
  <br>
  Give me ideas for scripts please!
</h2>
<p align="center">
  You can contact on discord @ its.Winter#6512 
  Or join my server below.
</p>
<h2 align="center">
  <a href="https://discord.gg/x58UEjb">
    <img src="https://discordapp.com/api/guilds/399500360877867018/widget.png?style=banner2" alt="Discord Server">
  </a>
</h2>
